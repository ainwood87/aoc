#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdbool.h>

typedef struct
{
    int label;
    int nextIdx;
    int prevIdx;
} NodeT;

#define N 1000000

//allocate N nodes in an array
static NodeT nodes[N];
static int curIdx = 0;

static bool labelInThree(int label, int nodeIdx)
{
    bool result = false;

    for (int i = 0; (i < 3) && !result; ++i)
    {
        const NodeT node = nodes[nodeIdx];
        result = (node.label == label);
        nodeIdx = node.nextIdx;
    }

    return result;
}

static int getDestIndex(int label)
{
    int idx;

    //Labels 1-9 are stored somewhere in indexes 0-8
    if (label < 10)
    {
        for (int i = 0; i <= 8; ++i)
        {
            if (nodes[i].label == label)
            {
                idx = i;
                break;
            }
        }
    }
    else
    {
        idx = label - 1;
    }

    return idx;
}

int main()
{
    for (int i = 0; i < N; ++i)
    {
        nodes[i].label      = i + 1;
        nodes[i].nextIdx    = i + 1;
        nodes[i].prevIdx    = i - 1;
    }

    //fix ends
    nodes[N - 1].nextIdx = 0;
    nodes[0].prevIdx = N - 1;

    //fix starting values: 784235916
    nodes[0].label = 7;
    nodes[1].label = 8;
    nodes[2].label = 4;
    nodes[3].label = 2;
    nodes[4].label = 3;
    nodes[5].label = 5;
    nodes[6].label = 9;
    nodes[7].label = 1;
    nodes[8].label = 6;

    //perform ten million iterations
    for (int iter = 0; iter < 10000000; ++iter, curIdx = nodes[curIdx].nextIdx)
    {
        //remove the three
        //first find the three, and the one after 
        const int cutIdx = nodes[curIdx].nextIdx;
        const int cut2Idx = nodes[cutIdx].nextIdx;
        const int cut3Idx = nodes[cut2Idx].nextIdx;
        const int cutAfterIdx = nodes[cut3Idx].nextIdx;

        //splice out the three
        nodes[curIdx].nextIdx = cutAfterIdx;
        nodes[cutAfterIdx].prevIdx = curIdx;

        //Determine the next label
        int destLabel = nodes[curIdx].label;

        do
        {
            --destLabel;
            if (destLabel < 1) 
            {
                destLabel = N;
            }
        } while (labelInThree(destLabel, cutIdx));

        // Determine the index of the node with destLabel. Based on how we store nodes, this is O(1)
        const int destIdx = getDestIndex(destLabel);
        const int destAfterIdx = nodes[destIdx].nextIdx;

        // Put the three after destIdx
        nodes[destIdx].nextIdx = cutIdx;
        nodes[cutIdx].prevIdx = destIdx;
        nodes[cut3Idx].nextIdx = destAfterIdx;
        nodes[destAfterIdx].prevIdx = cut3Idx;
    }

    NodeT cupOneNode = nodes[7];
    NodeT first = nodes[cupOneNode.nextIdx];
    NodeT second = nodes[first.nextIdx];
    uint64_t result = ((uint64_t) first.label) * ( (uint64_t) second.label);

    printf("result: %"PRIu64"\n", result);

    return 0;
}